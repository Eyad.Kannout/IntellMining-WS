package com.IntellMining.R;

import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;
import org.rosuda.JRI.Rengine;

import com.IntellMining.AbstractClasses.Parser;
import com.IntellMining.AbstractClasses.Wrapper;
import com.IntellMining.Helpers.FileManager;


public class RWrapper extends Wrapper{
//	private static Rengine engine = null;
//	public static Rengine getInstance() {
//	      if(engine == null) {
//	    	  engine = new Rengine(new String[] { "--no-save" }, false, null);
//	      }
//	      return engine;
//	   }
	static Rengine engine = new Rengine(new String[] { "--no-save" }, false, null);
	public RWrapper(Parser parser) throws Exception {
		this.parser = parser;
	}

	public RWrapper() throws Exception {
		
	}
	
	
	@Override
	protected UUID trainNaiveBayes(String trainingDataPath) throws Exception {
		UUID id = UUID.randomUUID();
		String savePath = FileManager.createFile(getMethod(), getTool(), id.toString());
//		Rengine engine = getInstance();
//		engine.eval("library(RMOA)");
//		engine.eval("dataStream <- datastream_csv(file=\""+ trainingDataPath.replace("\\", "\\\\\\\\") +"\" ,na.strings = \"\")");
//		engine.eval("ctrl <- MOAoptions(model = \"NaiveBayes\")");
//		engine.eval("mymodel <- NaiveBayes(control=ctrl)");
//		engine.eval("naiveBayesModel <- trainMOA(model = mymodel,formula = class ~ .,data = dataStream)");
		engine.eval("library (e1071)");
		engine.eval("trainingSet <- read.csv(\""+ trainingDataPath.replace("\\", "\\\\\\\\") +"\")");
    	String className = engine.eval("names(trainingSet)["+ attributeId +"]").asString();
    	engine.eval("naiveBayesModel <- naiveBayes(as.factor("+ className +") ~ ., data = trainingSet)");
    	engine.eval("save( naiveBayesModel, file = \""+ savePath.replace("\\", "\\\\\\\\") + "\")");
    	
		return id;
	}

	@Override
	protected UUID trainDecisionTree(String trainingDataPath) throws Exception {
		
		return null;
	}

	@Override
	protected Object testNaiveBayes() throws Exception {
		if (parser == null)
			throw new Exception("Parser is not set.");
		/* This code for JSON Processing
		engine.eval("con <- textConnection(csvDataString)");
		engine.eval("testing_csv <- read.csv(con)");
		engine.eval("close(con)");
		*/
		engine.eval("library (e1071)");
		String csvDataPath = (String) parser.Parse();
		engine.eval("testing_csv <- read.csv(\""+ csvDataPath.replace("\\", "\\\\\\\\") + "\")");
		
		String filePath = getFilePath();
		engine.eval("load(\"" + filePath.replace("\\", "\\\\\\\\") + "\")");
		engine.eval("result = predict(naiveBayesModel, testing_csv)");
		
		engine.eval("confusionMatrix = table(result, testing_csv$class)");
		engine.eval("accuracy = sum(diag(confusionMatrix)) / sum(confusionMatrix)").asDouble();
		double accuracy = engine.eval("accuracy").asDouble();
		
//		engine.eval("resultVector = as.vector(result)");
//		engine.eval("confusionMatrix = table(resultVector, testing_csv$class)");
//		engine.eval("accuracy = sum(diag(confusionMatrix)) / sum(confusionMatrix)");
//		double accuracy = engine.eval("accuracy").asDouble();
//		int numInstances = engine.eval("length(testing_csv$class)").asInt();
//		double numCorrect = accuracy * 1000;	
		
		JSONArray jsonArray = new JSONArray();
		JSONObject obj = new JSONObject();
		
		
//		double[] predictResults = engine.eval("predictResults").asDoubleArray();
//		double[] actualValues = engine.eval("actualValues").asDoubleArray();
//		engine.eval("rm(list = ls()");
//		engine.end();
//		
//		for(int i=0; i < numInstances; i++){
//			obj.put("Instance number" + i, "Actual Value = " + actualValues[i] + " -- Predicted Value = " + predictResults[i]);
//			if(predictResults[i] == actualValues[i])
//				numCorrect++;
//		}
//		jsonArray.put(obj);
//		obj = new JSONObject();
//		double accuracy = (double)numCorrect/numInstances * 100;
		
//		obj.put("numInstances", 1000);
//		obj.put("NumOfCorrect", numCorrect);
		obj.put("Accuracy", accuracy*100 + "%");
		jsonArray.put(obj);
		return jsonArray;
	}

	@Override
	protected Object testDecisionTree() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
