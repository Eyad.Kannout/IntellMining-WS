package com.IntellMining.R;


import com.IntellMining.AbstractClasses.Methods;
import com.IntellMining.AbstractClasses.Parser;
import com.IntellMining.AbstractClasses.Tools;

public class RParser extends Parser{
	
	
	public RParser(Tools tool, Methods method, String data) {
		this.testDataPath = data;
		this.method = method;
		this.tool = tool;
	}
	@Override
	protected Object ToNaiveBayes() throws Exception {
		/*  This code for JSON Processing
		DataSource source = new DataSource(testDataPath);
		Instances data = source.getDataSet();
	    data.setClassIndex(data.numAttributes()-1);
	    
	    String result = data.toString().replace("@relation TestSet1\n\n@attribute duration numeric\n@attribute protocol_type numeric\n@attribute service numeric\n@attribute flag numeric\n@attribute src_bytes numeric\n@attribute dst_bytes numeric\n@attribute land numeric\n@attribute wrong_fragment numeric\n@attribute urgent numeric\n@attribute hot numeric\n@attribute num_failed_logins numeric\n@attribute logged_in numeric\n@attribute num_compromised numeric\n@attribute root_shell numeric\n@attribute su_attempted numeric\n@attribute num_root numeric\n@attribute num_file_creations numeric\n@attribute num_shells numeric\n@attribute num_access_files numeric\n@attribute num_outbound_cmds numeric\n@attribute is_host_login numeric\n@attribute is_guest_login numeric\n@attribute count numeric\n@attribute srv_count numeric\n@attribute serror_rate numeric\n@attribute srv_serror_rate numeric\n@attribute rerror_rate numeric\n@attribute srv_rerror_rate numeric\n@attribute same_srv_rate numeric\n@attribute diff_srv_rate numeric\n@attribute srv_diff_host_rate numeric\n@attribute dst_host_count numeric\n@attribute dst_host_srv_count numeric\n@attribute dst_host_same_srv_rate numeric\n@attribute dst_host_diff_srv_rate numeric\n@attribute dst_host_same_src_port_rate numeric\n@attribute dst_host_srv_diff_host_rate numeric\n@attribute dst_host_serror_rate numeric\n@attribute dst_host_srv_serror_rate numeric\n@attribute dst_host_rerror_rate numeric\n@attribute dst_host_srv_rerror_rate numeric\n@attribute class numeric\n\n@data", "duration,protocol_type,service,flag,src_bytes,dst_bytes,land,wrong_fragment,urgent,hot,num_failed_logins,logged_in,num_compromised,root_shell,su_attempted,num_root,num_file_creations,num_shells,num_access_files,num_outbound_cmds,is_host_login,is_guest_login,count,srv_count,serror_rate,srv_serror_rate,rerror_rate,srv_rerror_rate,same_srv_rate,diff_srv_rate,srv_diff_host_rate,dst_host_count,dst_host_srv_count,dst_host_same_srv_rate,dst_host_diff_srv_rate,dst_host_same_src_port_rate,dst_host_srv_diff_host_rate,dst_host_serror_rate,dst_host_srv_serror_rate,dst_host_rerror_rate,dst_host_srv_rerror_rate,class");
	    
	    StringBuilder builder = new StringBuilder();
	    for (int i = 0; i < data.numAttributes(); i++) {
	    	builder.append(data.attribute(i).name()+',');
		}
	    builder.append('\n');
	    Object[] lines = data.toArray();
		
		for(Object line : lines) {
		    builder.append(line.toString()+'\n');
		}
		String result = builder.toString();
		*/
		
		return testDataPath;
	}

	@Override
	protected Object ToDecisionTree() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
