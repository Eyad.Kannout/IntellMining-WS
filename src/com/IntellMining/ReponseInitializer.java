package com.IntellMining;

import javax.ws.rs.core.Response;

public class ReponseInitializer {
	public Response InternalError(Exception ex)
	{
		return Response.status(500).entity("Server was not able to build the model\n" + ex.toString()).build();
	}
	
	public  Response OK(String output)
	{
		return Response.ok(output).build();
	}
	
	public Response NotFound(String id)
	{
		return Response.status(404).entity("No trained Model was found for the following id:\n" + id).build();
	}
}
