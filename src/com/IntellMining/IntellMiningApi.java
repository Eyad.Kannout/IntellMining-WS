package com.IntellMining;


import java.util.UUID;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import com.IntellMining.AbstractClasses.*;
import com.IntellMining.Factory.*;
import com.IntellMining.Helpers.FileManager;

@Path("intell")
public class IntellMiningApi extends ReponseInitializer {

	Logger logger = LoggerFactory.getLogger("com.IntellMining.IntellMiningApi");
	
	@POST
	@Path("Test/{toolId}/{methodId}/{attributeId}/{id}")
	public Response Test(@PathParam("toolId") int toolId,
			@PathParam("methodId") int methodId,
			@PathParam("attributeId") int attributeId,
			@PathParam("id") String modelId, String testDataPath) {

		Methods method = Methods.getMethod(methodId);
		Tools tool = Tools.getTool(toolId);
		logger.warn("Test Request is running");
		try {

			if (!FileManager.checkFile(method, tool, modelId)) {
				return NotFound(modelId);
			}
			
			Parser parser = ParserFactory.createParser(method, tool, testDataPath);
			
			Wrapper wrapper = WrapperFactory.createWrapper(parser);
			
			JSONArray jsonArray = (JSONArray) wrapper.test(attributeId, modelId);
			JSONObject status = new JSONObject();
			status.put("Status", 200);
			jsonArray.put(status);
			return OK(jsonArray.toString());

		} catch (NotImplementedException ex) {
			return OK("The method " + method.toString()
					+ "is not supported on " + tool.toString());
		} catch (Exception e) {
			return InternalError(e);
		}
	}

	@POST
	@Path("Train/{toolId}/{methodId}/{attributeId}")
	public Response Train(@PathParam("toolId") int toolId,
			@PathParam("methodId") int methodId,
			@PathParam("attributeId") int attributeId, String trainingDataPath) {

		Methods method = Methods.getMethod(methodId);
		Tools tool = Tools.getTool(toolId);
		
		try {
			Parser parser = ParserFactory.createParser(method, tool, null);
			Wrapper wrapper = WrapperFactory.createWrapper(parser);
			UUID testId = wrapper.train(trainingDataPath, attributeId);
			
			return OK(testId.toString());

		} catch (Exception e) {
			return InternalError(e);
		}
	}
}
