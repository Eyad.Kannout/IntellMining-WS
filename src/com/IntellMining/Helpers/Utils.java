package com.IntellMining.Helpers;

import javax.ws.rs.core.Response;

public class Utils {
	public static Response ReturnInternalError(Exception ex)
	{
		return Response.status(500).entity("Server was not able to build the model\n" + ex.toString()).build();
	}
	
	public static Response ReturnOK(String output)
	{
		return Response.status(200).entity(output).build();
	}
}
