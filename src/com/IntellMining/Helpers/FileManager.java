package com.IntellMining.Helpers;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import com.IntellMining.AbstractClasses.Methods;
import com.IntellMining.AbstractClasses.Tools;

public class FileManager {

	private static String ROOT_DIR = "C:\\IntellMiningFiles";
	
	private static Path getDirectory(Tools tool)
	{
		return Paths.get(ROOT_DIR, tool.toString());
	}
	
	public static String createFile(Methods method, Tools tool, String id)
	{
		Path dir = getDirectory(tool);
		return Paths.get(dir.toString(),method.toString() + "_" + id).toString();
	}
	
	public static String getFilePath(Methods method, Tools tool, String id)
	{
		Path dir = getDirectory(tool);
		return Paths.get(dir.toString(),method.toString() + "_" + id).toString();
	}
	
	public static boolean checkFile(Methods method, Tools tool, String id)
	{   
		Path dir = getDirectory(tool);
		File file = new File(Paths.get(dir.toString(),method.toString() + "_" + id).toString());
		return file.exists();
	}
}
