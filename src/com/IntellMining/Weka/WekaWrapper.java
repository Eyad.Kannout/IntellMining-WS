package com.IntellMining.Weka;


import java.io.File;
import java.util.ArrayList;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;
import com.IntellMining.AbstractClasses.Parser;
import com.IntellMining.AbstractClasses.Wrapper;
import com.IntellMining.Helpers.FileManager;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.EvaluationUtils;
import weka.classifiers.evaluation.Prediction;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class WekaWrapper extends Wrapper {

	public WekaWrapper(Parser parser) throws Exception {
		this.parser = parser;
	}

	public WekaWrapper() throws Exception {
	}

	@Override
	protected Object testNaiveBayes() throws Exception {

		if (parser == null)
			throw new Exception("Parser is not set.");

		Instances instances = (Instances) parser.Parse();
		Attribute attribute  = instances.attribute(attributeId);
		instances.setClass(attribute );
		NaiveBayes nb = (NaiveBayes) weka.core.SerializationHelper.read(getFilePath());
		EvaluationUtils ev = new EvaluationUtils();  // I used EvaluationUtils because it needs only the model to predict
													 //unlike Evaluation which needs the whole data set
		ArrayList<Prediction> v = ev.getTestPredictions(nb, instances);
		JSONArray jsonArray = new JSONArray();
		JSONObject obj = new JSONObject();
		int numCorrect = 0;
		double accuracy = 0;
		int numInstances = 0;
		for(Prediction pred : v)
		{
			numInstances++;
//			obj.put("Instance number" + numInstances, "Actual Value = " + pred.actual() + " -- Predicted Value = " + pred.predicted());
			if (pred.actual() == pred.predicted())
			{
				numCorrect++;
			}
		}
//		jsonArray.put(obj);
//		obj = new JSONObject();
		accuracy = (double)numCorrect/numInstances * 100;
		obj.put("NumofInstances", numInstances);
		obj.put("NumofCorrect", numCorrect);
		obj.put("Accuracy", accuracy + "%");
		jsonArray.put(obj);

		return jsonArray;
	}

	@Override
	protected UUID trainNaiveBayes(String trainingDataPath) throws Exception {
		
		NaiveBayes nb = buildClassifier(trainingDataPath);		
		UUID id = UUID.randomUUID();
		String savePath = FileManager.createFile(getMethod(), getTool(), id.toString());
		File file = new File(savePath);
		file.createNewFile();
		weka.core.SerializationHelper.write(savePath, nb);
		return id;
	}

	private NaiveBayes buildClassifier(String trainingDataPath) throws Exception {
		try {
			DataSource source = new DataSource(trainingDataPath);
			Instances train = source.getDataSet();
		
			Attribute attribute = train.attribute(this.attributeId);
			if(attribute == null)
				throw new Exception("Attribute not found.");
			
			train.setClass(attribute);
			NaiveBayes nb = new NaiveBayes();
			nb.buildClassifier(train);
			return nb;
		} catch (Exception e1) {
			throw e1;
		}
		
	}

	@Override
	protected UUID trainDecisionTree(String trainingDataPath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Object testDecisionTree() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
