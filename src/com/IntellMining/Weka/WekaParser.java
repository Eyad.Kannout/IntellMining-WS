package com.IntellMining.Weka;


import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import com.IntellMining.AbstractClasses.*;

public class WekaParser extends Parser {

	public WekaParser(Tools tool, Methods method, String data) {
		this.testDataPath = data;
		this.method = method;
		this.tool = tool;
	}

	@Override
	protected Object ToNaiveBayes() throws Exception {
		DataSource source = new DataSource(testDataPath);
		Instances instances = source.getDataSet();
		return instances;
		
		//InputStream stream = new StringBufferInputStream(this.testDataPath);
//		File testFile = new File(this.testDataPath);
//		JSONLoader loader = new JSONLoader();
//		loader.setSource(testFile);
//		//loader.setSource(stream);
//		return loader.getDataSet();
	}

	@Override
	protected Object ToDecisionTree() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
