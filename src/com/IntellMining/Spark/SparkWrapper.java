package com.IntellMining.Spark;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.classification.NaiveBayes;
import org.apache.spark.mllib.classification.NaiveBayesModel;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.util.MLUtils;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.IntellMining.AbstractClasses.Parser;
import com.IntellMining.AbstractClasses.Wrapper;
import com.IntellMining.Helpers.FileManager;

public class SparkWrapper extends Wrapper implements Serializable{
	
//	private static transient JavaSparkContext sc = ssc. sparkContext();
	private static transient JavaSparkContext sc = new JavaSparkContext("local","My app");

//	static transient SparkConf sparkConf = new SparkConf().setAppName("My app").setMaster("local[5]").
//			set("spark.driver.allowMultipleContexts", "true");
//	static transient JavaStreamingContext ssc = new JavaStreamingContext(sparkConf, Durations.milliseconds(500));
	public SparkWrapper(Parser parser) throws Exception {
		this.parser = parser;
	}
	
	protected UUID trainNaiveBayes(String trainingDataPath) throws Exception {
		
//		JavaRDD<String> data = sc.textFile(trainingDataPath);
		
		JavaRDD<String> data = sc.textFile(trainingDataPath);
		JavaRDD<LabeledPoint> trainData = data.map(new Function<String, LabeledPoint>() {
			public LabeledPoint call(String line) throws Exception {
				List<String> featureList = Arrays.asList(line.trim().split(","));
				double[] points = new double[featureList.size()-1];
				double classLabel = Double.parseDouble(featureList.get(featureList.size() - 1));
				for (int i = 0; i < featureList.size()-1; i++){
					points[i] = Double.parseDouble(featureList.get(i));
				}
				
				return new LabeledPoint(classLabel, Vectors.dense(points));
			}
		});
		UUID id = UUID.randomUUID();
		String savePath = FileManager.createFile(getMethod(), getTool(), id.toString());
//		System.out.println(savePath);
		final NaiveBayesModel model = NaiveBayes.train(trainData.rdd(), 1.0);
		model.save(sc.sc(), savePath);
		
		return id;
	}

	@Override
	protected UUID trainDecisionTree(String trainingDataPath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Object testNaiveBayes() throws Exception {
//		SparkConf sparkConf = new SparkConf().setAppName("My app").setMaster("local").
//		set("spark.driver.allowMultipleContexts", "true");
//		JavaSparkContext sc = new JavaSparkContext(sparkConf);
		
		if (parser == null)
			throw new Exception("Parser is not set.");
		String filePath = getFilePath();
		final NaiveBayesModel savedModel = NaiveBayesModel.load(sc.sc(), filePath);
//		final NaiveBayesModel savedModel = NaiveBayesModel.load(sc.sc(), filePath);
		/* This code for JSON Processing
		List<String> csvData = (List<String>) parser.Parse();
		JavaRDD<String> data = sc.parallelize(csvData);
		*/
		
//		JavaRDD<LabeledPoint> testData = MLUtils.loadLabeledPoints(SparkParser.ssc.sparkContext().sc(), (String) parser.Parse()).toJavaRDD();
//		JavaRDD<LabeledPoint> testData = MLUtils.loadLabeledPoints(sc.sc(), (String) parser.Parse()).toJavaRDD();
		 
		/* This Part is for transforming the data into Spark Format */
		JavaRDD<String> data = sc.textFile((String) parser.Parse());
		JavaRDD<LabeledPoint> testData = data.map(new Function<String, LabeledPoint>() {
			public LabeledPoint call(String line) throws Exception {
				List<String> featureList = Arrays.asList(line.trim().split(","));
				double[] points = new double[featureList.size()-1];
				double classLabel = Double.parseDouble(featureList.get(featureList.size() - 1));
				for (int i = 0; i < featureList.size()-1; i++){
					points[i] = Double.parseDouble(featureList.get(i));
				}
				
				return new LabeledPoint(classLabel, Vectors.dense(points));
			}
		});
		
		
//		JavaRDD<LabeledPoint> testData = (JavaRDD<LabeledPoint>) parser.Parse();
		
		JavaPairRDD<Double, Double> predictionAndLabel = testData.mapToPair(new PairFunction<LabeledPoint, Double, Double>() {
			public Tuple2<Double, Double> call(LabeledPoint p) {
				  return new Tuple2<Double, Double>(savedModel.predict(p.features()), p.label());
				    }
		});
	
		
		double accuracy = predictionAndLabel.filter(new Function<Tuple2<Double, Double>, Boolean>() {
		    public Boolean call(Tuple2<Double, Double> pl) throws JSONException {
		      
		      return pl._1().equals(pl._2());
		    }
		  }).count() / (double) testData.count();
		
		JSONArray jsonArray = new JSONArray();
		JSONObject obj = new JSONObject();
		
//		List<Tuple2<Double, Double>> res = predictionAndLabel.collect(); 
//    	for (int i=0; i<res.size();i++){
//    		obj.put("Instance number" + i , "Actual Value = " + res.get(i)._2 + " -- Predicted Value = " +  res.get(i)._1);	
//    	}
    	
//		jsonArray.put(obj);
//		obj = new JSONObject();
//		obj.put("NumofInstances", testData.count());
//		obj.put("NumOfCorrect", accuracy * testData.count());
		obj.put("Accuracy", accuracy*100 + "%");
		jsonArray.put(obj);
			
		return jsonArray;
	}

	@Override
	protected Object testDecisionTree() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
