package com.IntellMining.Spark;

import java.io.Serializable;
import com.IntellMining.AbstractClasses.Methods;
import com.IntellMining.AbstractClasses.Parser;
import com.IntellMining.AbstractClasses.Tools;

public class SparkParser extends Parser implements Serializable{

//	static transient SparkConf sparkConf = new SparkConf().setAppName("My app").setMaster("local[4]").
//			set("spark.driver.allowMultipleContexts", "true");
//	static transient JavaStreamingContext ssc = new JavaStreamingContext(sparkConf, Durations.milliseconds(500));
	
	public SparkParser(Tools tool, Methods method, String data) {
		this.testDataPath = data;
		this.method = method;
		this.tool = tool;
	}
	@Override
	protected Object ToNaiveBayes() throws Exception {
		/* This code for JSON Processing
		DataSource source = new DataSource(testDataPath);
		Instances data = source.getDataSet();
	    data.setClassIndex(data.numAttributes()-1);  
		List<String> csvData = new ArrayList<String>();
		Object[] rows = data.toArray();
		for (int i = 0; i < rows.length; i++) {
			csvData.add(rows[i].toString());
	    }
		*/
		
		/* This Part is for transforming the data into Spark Format */
//		JavaRDD<String> data = ssc.sparkContext().textFile(testDataPath);
//		JavaRDD<LabeledPoint> testData = data.map(new Function<String, LabeledPoint>() {
//			public LabeledPoint call(String line) throws Exception {
//				List<String> featureList = Arrays.asList(line.trim().split(","));
//				double[] points = new double[featureList.size()-1];
//				double classLabel = Double.parseDouble(featureList.get(featureList.size() - 1));
//				for (int i = 0; i < featureList.size()-1; i++){
//					points[i] = Double.parseDouble(featureList.get(i));
//				}
//				
//				return new LabeledPoint(classLabel, Vectors.dense(points));
//			}
//		});
//		
		
		return testDataPath;
	}

	@Override
	protected Object ToDecisionTree() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
