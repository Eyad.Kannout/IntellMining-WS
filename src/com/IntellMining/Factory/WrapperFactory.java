package com.IntellMining.Factory;

import com.IntellMining.AbstractClasses.Parser;
import com.IntellMining.AbstractClasses.Wrapper;
import com.IntellMining.MOA.MOAWrapper;
import com.IntellMining.R.RWrapper;
import com.IntellMining.Spark.SparkWrapper;
import com.IntellMining.Weka.WekaWrapper;


public class WrapperFactory {
	
	public static Wrapper createWrapper(Parser parser) throws Exception{
		
		switch (parser.getTool()) {
		case WEKA:
			return new WekaWrapper(parser);
		case MOA:
			return new MOAWrapper(parser);
		case R:
			return new RWrapper(parser);
		case SPARK:
			return new SparkWrapper(parser);
		default:
			break;
		}
		return null;
		
		
	}

}
