package com.IntellMining.Factory;

import com.IntellMining.AbstractClasses.Methods;
import com.IntellMining.AbstractClasses.Parser;
import com.IntellMining.AbstractClasses.Tools;
import com.IntellMining.MOA.MOAParser;
import com.IntellMining.R.RParser;
import com.IntellMining.Spark.SparkParser;
import com.IntellMining.Weka.WekaParser;

public class ParserFactory {

	public static Parser createParser(Methods method, Tools tool, String testDataPath) throws Exception {
		switch (tool) {
		case WEKA:
			return new WekaParser(tool, method, testDataPath);
		case MOA:
			return new MOAParser(tool, method, testDataPath);
		case R:
			return new RParser(tool, method, testDataPath);
		case SPARK:
			return new SparkParser(tool, method, testDataPath);
		default:
			throw new Exception("Tool not found");
		}
	}

}
