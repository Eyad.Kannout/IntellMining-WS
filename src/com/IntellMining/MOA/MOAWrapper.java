package com.IntellMining.MOA;

import java.io.File;
import java.util.UUID;

import org.apache.commons.lang.NotImplementedException;
import org.json.JSONArray;
import org.json.JSONObject;

import moa.streams.CachedInstancesStream;
import moa.tasks.EvaluateModel;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import moa.classifiers.bayes.*;
import moa.core.Measurement;
import moa.evaluation.LearningEvaluation;

import com.IntellMining.AbstractClasses.Parser;
import com.IntellMining.AbstractClasses.Wrapper;
import com.IntellMining.Helpers.FileManager;

public class MOAWrapper extends Wrapper {
	
	public MOAWrapper(Parser parser) throws Exception {
		this.parser = parser;
	}

	public MOAWrapper() throws Exception {
	}

	@Override
	protected UUID trainNaiveBayes(String trainingDataPath) throws Exception {
		
		NaiveBayes nb = buildClassifier(trainingDataPath);		
		UUID id = UUID.randomUUID();
		String savePath = FileManager.createFile(getMethod(), getTool(), id.toString());
		File file = new File(savePath);
		file.createNewFile();
		moa.core.SerializeUtils.writeToFile(file, nb);
		return id;
	}
	
	private NaiveBayes buildClassifier(String trainingDataPath) throws Exception {
		try { 
			 
//		     ArffFileStream MOAStream = new ArffFileStream(); // -1 means the class attribute is the last one
//		     MOAStream.arffFileOption.setValue(trainingDataPath);
//		     MOAStream.classIndexOption.setValue(this.attributeId);
//		     MOAStream.prepareForUse();
			 DataSource source = new DataSource(trainingDataPath);
			 Instances train = source.getDataSet();
			 train.setClassIndex(this.attributeId);
		     CachedInstancesStream stream = new CachedInstancesStream(train);
		     
		     NaiveBayes nb = new NaiveBayes();
		     nb.setModelContext(stream.getHeader());
		     nb.trainingHasStarted();
		     nb.prepareForUse();
		     
		     while (stream.hasMoreInstances()){
		         Instance trainInst = stream.nextInstance();
		         nb.trainOnInstance(trainInst);
		     }
		     
		     return nb;
		
		} catch (Exception e1) {
			throw e1;
		}
	}

	@Override
	protected Object testNaiveBayes() throws Exception {
		
		if (parser == null)
			throw new Exception("Parser is not set.");
		
		File file = new File(getFilePath());
		NaiveBayes nb = (NaiveBayes) moa.core.SerializeUtils.readFromFile(file);
		nb.prepareForUse();
		
		// we should define the class index before make any prediction
		Instances instances = (Instances) parser.Parse();
		instances.setClassIndex(this.attributeId);
		CachedInstancesStream streamInstances =  new CachedInstancesStream(instances);
		
		EvaluateModel eval = new EvaluateModel();
		eval.modelOption.setCurrentObject(nb);
		eval.streamOption.setCurrentObject(streamInstances);
		eval.maxInstancesOption.setValue(instances.numInstances());
		eval.prepareForUse();
		LearningEvaluation res = (LearningEvaluation) eval.doTask();
		Measurement[] measurements = res.getMeasurements();
		
		JSONArray jsonArray = new JSONArray();
		JSONObject obj = new JSONObject();
//		obj.put("NumofInstances",measurements[0].toString());
		obj.put("Accuracy",measurements[1].toString() + "%");
		jsonArray.put(obj);
		
		
		
//		JSONArray jsonArray = new JSONArray();
//		JSONObject obj = new JSONObject();
//		int numCorrect = 0;
//		double accuracy = 0;
//		int numInstances = 0;
		
//		while(streamInstances.hasMoreInstances()){
//			numInstances++;
//			Instance inst = streamInstances.nextInstance();
//			
//			if (nb.correctlyClassifies(inst)){
//				numCorrect++;
//				obj.put("Instance number" + numInstances, "True Prediction");	
//			}
//			else{
//				obj.put("Instance number" + numInstances, "False Prediction");
//			}
//		}
//		jsonArray.put(obj);
//		obj = new JSONObject();
//		accuracy = (double)numCorrect/numInstances * 100;
//		obj.put("NumofCorrect", numCorrect);
//		obj.put("Accuracy", accuracy + "%");
//		jsonArray.put(obj);
		
		return jsonArray;
		
	}

	@Override
	protected UUID trainDecisionTree(String trainingDataPath) throws Exception {
		
		
		throw new NotImplementedException();
	}

	@Override
	protected Object testDecisionTree() throws Exception {
		
		return null;
	}
	
	

}
