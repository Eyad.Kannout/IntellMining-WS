package com.IntellMining.MOA;


import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import com.IntellMining.AbstractClasses.*;

public class MOAParser extends Parser {
	
	public MOAParser(Tools tool, Methods method, String data) {
		this.testDataPath = data;
		this.method = method;
		this.tool = tool;
	}
	@Override
	protected Object ToNaiveBayes() throws Exception {
		DataSource source = new DataSource(testDataPath);
		Instances instances = source.getDataSet();
		return instances;
	}

	@Override
	protected Object ToDecisionTree() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
