package com.IntellMining.AbstractClasses;

public enum Tools {
	WEKA(1), MOA (2), R(3), SPARK(4);
	
    private final int id;
    Tools(int id) { this.id = id; }
    public int getValue() { return id; }
    
    public static Tools getTool(int index) {
        for (Tools l : Tools.values()) {
            if (l.id == index) 
            	return l;
        }
        throw new IllegalArgumentException("Tool not found. Amputated?");
     }
}
