package com.IntellMining.AbstractClasses;

import java.util.UUID;

import com.IntellMining.Helpers.FileManager;

public abstract class Wrapper {

	protected Parser parser;

	private String modelId;
	protected int attributeId;

	protected Methods getMethod() {
		return parser.getMethod();
	}

	protected Tools getTool() {
		return parser.getTool();
	}

	public UUID train(String trainingDataPath, int attributeId) throws Exception {
		
		this.attributeId = attributeId;
		switch (getMethod()) {		
		case NAIVE_BAYES:
			return trainNaiveBayes(trainingDataPath);
		case DECISION_TREE:
			return trainDecisionTree(trainingDataPath);
		default:
			throw new Exception("Method not found.");
		}
	}

	protected abstract UUID trainNaiveBayes(String trainingDataPath)
			throws Exception;
	
	protected abstract UUID trainDecisionTree(String trainingDataPath)
			throws Exception;

	protected abstract Object testNaiveBayes() throws Exception;
	
	protected abstract Object testDecisionTree() throws Exception;

	public Object test(int attributeId, String modelId) throws Exception {

		this.modelId = modelId;
		this.attributeId = attributeId;

		switch (getMethod()) {
		case NAIVE_BAYES:
			return testNaiveBayes();
		case DECISION_TREE:
			return testDecisionTree();
		default:
			throw new Exception("Method not found.");
		}
	}

	protected String getFilePath() {
		return FileManager.getFilePath(getMethod(), getTool(), this.modelId);
	}
}
