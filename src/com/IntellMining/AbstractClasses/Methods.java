package com.IntellMining.AbstractClasses;


public enum Methods {
	NAIVE_BAYES(1), NEURAL_NETWORK(2), DECISION_TREE(3);
	
    private final int id;
    Methods(int id) { this.id = id; }
    public int getValue() { return id; }
    
    public static Methods getMethod(int index) {
        for (Methods l : Methods.values()) {
            if (l.id == index) return l;
        }
        throw new IllegalArgumentException("Method not found. Amputated?");
     }
}
