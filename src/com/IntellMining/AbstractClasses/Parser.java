package com.IntellMining.AbstractClasses;

public abstract class Parser {
	protected Tools tool;
	protected Methods method;
	protected String testDataPath;
	
	public Tools getTool()
	{
		return tool;
	}
	
	public Methods getMethod()
	{
		return method;
	}
	
	protected abstract Object ToNaiveBayes() throws Exception;
	
	protected abstract Object ToDecisionTree() throws Exception;
	
	public Object Parse() throws Exception {
		switch (method) {
		case NAIVE_BAYES:
			return ToNaiveBayes();
		case DECISION_TREE:
			return ToDecisionTree();
		default:
			throw new Exception("No method found.");
		}
	}
}
